<?php

namespace Drupal\Tests\nodeorder\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests user permissions.
 *
 * @group nodeorder
 */
class NodeorderPermissionsTest extends BrowserTestBase {

  use TaxonomyTestTrait {
    createVocabulary as drupalCreateVocabulary;
    createTerm as drupalCreateTerm;
  }

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static $modules = ['taxonomy', 'nodeorder'];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    $this->configFactory = $this->container->get('config.factory');
  }

  /**
   * Tests viewing a module configuration form.
   */
  public function testViewModuleConfigurationForm() {
    $url = Url::fromRoute('nodeorder.admin');

    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(403);

    $user = $this->drupalCreateUser();
    $this->drupalLogin($user);
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(403);

    $user = $this->drupalCreateUser(['administer nodeorder']);
    $this->drupalLogin($user);
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', "//form[@id='nodeorder-admin']");
  }

  /**
   * Tests viewing a term's nodes order page.
   */
  public function testViewOrderNodesPageInTerm() {
    // Orderable vocabulary.
    $vocabulary1 = $this->drupalCreateVocabulary();
    $this->configFactory->getEditable('nodeorder.settings')
      ->set('vocabularies', [$vocabulary1->id() => TRUE])
      ->save();
    $term1 = $this->drupalCreateTerm($vocabulary1);

    $vocabulary2 = $this->drupalCreateVocabulary();
    $term2 = $this->drupalCreateTerm($vocabulary2);

    $term1_order_url = Url::fromRoute('nodeorder.admin_order', ['taxonomy_term' => $term1->id()]);
    $term2_order_url = Url::fromRoute('nodeorder.admin_order', ['taxonomy_term' => $term2->id()]);

    $this->drupalGet($term1_order_url);
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($term2_order_url);
    $this->assertSession()->statusCodeEquals(403);

    $user = $this->drupalCreateUser(['administer taxonomy']);
    $this->drupalLogin($user);
    $this->drupalGet($term1_order_url);
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($term2_order_url);
    $this->assertSession()->statusCodeEquals(403);

    $user = $this->drupalCreateUser(['administer taxonomy', 'order nodes within categories']);
    $this->drupalLogin($user);
    $this->drupalGet($term1_order_url);
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($term2_order_url);
    $this->assertSession()->statusCodeEquals(403);
  }

}
