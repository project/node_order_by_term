<?php

namespace Drupal\Tests\nodeorder\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\SchemaCheckTestTrait;

/**
 * Tests the nodeorder config schema.
 *
 * @group nodeorder
 */
class NodeorderConfigSchemaTest extends KernelTestBase {

  use SchemaCheckTestTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = ['system', 'nodeorder'];

  /**
   * The typed config handler.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfig;

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $configuration;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->typedConfig = $this->container->get('config.typed');
    $this->configuration = $this->container->get('config.nodeorder_settings');
  }

  /**
   * Tests config schema.
   *
   * @dataProvider configDataProvider
   */
  public function testConfigSchema($data, $expected) {
    $config = $this->configuration->setData($data);

    $actual = TRUE;
    $message = NULL;
    try {
      $this->assertConfigSchema($this->typedConfig, $config->getName(), $config->get());
    }
    catch (\Throwable $exception) {
      $actual = FALSE;
      $message = $exception->getMessage();
    }

    $this->assertEquals($expected, $actual, $message);
  }

  /**
   * {@inheritdoc}
   */
  public function configDataProvider() {
    return [
      [
        [
          'vocabularies' => [],
          'override_taxonomy_page' => TRUE,
          'show_links_on_node' => TRUE,
          'link_to_ordering_page' => TRUE,
          'link_to_ordering_page_taxonomy_admin' => TRUE,
        ],
        TRUE,
      ],
      [
        [
          'vocabularies' => new \stdClass(),
          'override_taxonomy_page' => [],
          'show_links_on_node' => 1,
          'link_to_ordering_page' => 0,
          'link_to_ordering_page_taxonomy_admin' => $this->randomString(),
        ],
        FALSE,
      ]
    ];
  }

}
